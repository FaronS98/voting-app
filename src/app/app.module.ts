import {NgModule} from '@angular/core';
import {BrowserModule} from '@angular/platform-browser';

import {AppComponent} from './app.component';
import {VotersComponent} from './pages/voters/voters.component';
import {CandidatesComponent} from './pages/candidates/candidates.component';
import {AddVoteComponent} from './pages/add-vote/add-vote.component';
import {InputModule} from "./components/input/input.module";
import {TableModule} from "./components/table/table.module";
import {AppRoutingModule} from "./app.router";
import {AppHeaderComponent} from './components/app-header/app-header.component';
import {ModalModule} from "./components/modal/modal.module";
import {NotifierModule} from "angular-notifier";
import {MatSelectModule} from '@angular/material/select'
import {MatInputModule} from "@angular/material/input";
import {BrowserAnimationsModule} from "@angular/platform-browser/animations";
import {FormsModule} from "@angular/forms";

@NgModule({
  declarations: [
    AppComponent,
    VotersComponent,
    CandidatesComponent,
    AddVoteComponent,
    AppHeaderComponent,
  ],
  imports: [
    BrowserAnimationsModule,
    BrowserModule,
    AppRoutingModule,
    InputModule,
    TableModule,
    ModalModule,
    NotifierModule.withConfig({}),
    MatSelectModule,
    MatInputModule,
    FormsModule,
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule {
}
