import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {AddVoteComponent} from "./pages/add-vote/add-vote.component";
import {VotersComponent} from "./pages/voters/voters.component";
import {CandidatesComponent} from "./pages/candidates/candidates.component";

const routes: Routes = [
  {path: 'add-vote', component: AddVoteComponent},
  {path: 'voters', component: VotersComponent},
  {path: 'candidates', component: CandidatesComponent},
  {path: '**', redirectTo: '/add-vote'}
];

@NgModule({
  imports: [RouterModule.forRoot(routes, {scrollPositionRestoration: 'enabled'})],
  exports: [RouterModule]
})
export class AppRoutingModule {
}
