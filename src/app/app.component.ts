import {Component} from '@angular/core';
import {MenuInterface} from "./core/interfaces/menu.interface";

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {

  mainMenu: MenuInterface[] = [
    {
      name: 'Add vote',
      routerLink: '/add-vote',
    },
    {
      name: 'Voters',
      routerLink: '/voters',
    },
    {
      name: 'Candidates',
      routerLink: '/candidates',
    }
  ]
}
