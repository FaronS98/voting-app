import {Component, OnInit, ViewChild} from '@angular/core';
import {VotersInterface} from "../../core/interfaces/voters.interface";
import {TableSettingsInterface} from "../../core/interfaces/table-settings.interface";
import {VotesService} from "../../core/services/votes.service";
import {FormBuilder, Validators} from "@angular/forms";
import {ModalComponent} from "../../components/modal/modal.component";
import {validateAllFormFields} from "../../core/validators/validators";

@Component({
  selector: 'app-voters',
  templateUrl: './voters.component.html',
  styleUrls: ['./voters.component.scss']
})
export class VotersComponent implements OnInit {

  @ViewChild('addNewVoterModal', {static: true}) addNewVoterModal: ModalComponent;
  public settingData: TableSettingsInterface[] = [
    {
      title: 'Name',
      value: 'name',
      minWidth: '200px',
    },
    {
      title: 'Has voted',
      value: 'hasVoted',
      minWidth: '80px',
    },
  ];
  public voters: VotersInterface[];
  public addVoter = this.fb.group({
    id: [null],
    name: [null, Validators.required],
    hasVoted: [null],
  })

  constructor(public votesService: VotesService,
              private fb: FormBuilder) {
  }

  ngOnInit() {
    this.getVoters();
  }

  private getVoters() {
    this.voters = this.votesService.getVoters();
  }

  public prepareAddNewVoterModal(): void {
    this.addVoter.get('id')?.patchValue(this.voters.length + 1)
    this.addVoter.get('hasVoted')?.patchValue(false)
    this.addNewVoterModal.open();
  }

  public addNewVoter() {
    if (!this.addVoter.valid) {
      validateAllFormFields(this.addVoter);
      return;
    }
    this.votesService.saveVoter(this.addVoter.value);
    this.addNewVoterModal.close();
  }
}
