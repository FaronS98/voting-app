import {Component, OnInit, ViewChild} from '@angular/core';
import {TableSettingsInterface} from "../../core/interfaces/table-settings.interface";
import {VotesService} from "../../core/services/votes.service";
import {CandidatesInterface} from "../../core/interfaces/candidates.interface";
import {FormBuilder, Validators} from "@angular/forms";
import {ModalComponent} from "../../components/modal/modal.component";
import {validateAllFormFields} from "../../core/validators/validators";

@Component({
  selector: 'app-candidates',
  templateUrl: './candidates.component.html',
  styleUrls: ['./candidates.component.scss']
})
export class CandidatesComponent implements OnInit {

  @ViewChild('addNewCandidateModal', {static: true}) addNewCandidateModal: ModalComponent;
  public settingData: TableSettingsInterface[] = [
    {
      title: 'Name',
      value: 'name',
      minWidth: '200px'
    },
    {
      title: 'Surname',
      value: 'surname',
      minWidth: '200px',
    },
    {
      title: 'Votes',
      value: 'votes',
      minWidth: '80px',
    },
  ];
  public candidates: CandidatesInterface[];
  public addCandidate = this.fb.group({
    id: [null],
    name: [null, Validators.required],
    surname: [null, Validators.required],
    votes: [null],
  })

  constructor(public votesService: VotesService,
              private fb: FormBuilder) {
  }

  ngOnInit(){
    this.getVoters();
  }

  private getVoters(){
    this.candidates = this.votesService.getCandidates();
  }

  public prepareAddNewCandidateModal(): void {
    this.addCandidate.get('id')?.patchValue(this.candidates.length + 1)
    this.addCandidate.get('votes')?.patchValue(0)
    this.addNewCandidateModal.open();
  }

  public addNewCandidate() {
    if (!this.addCandidate.valid) {
      validateAllFormFields(this.addCandidate);
      return;
    }
    this.votesService.saveCandidate(this.addCandidate.value);
    this.addNewCandidateModal.close();
  }
}
