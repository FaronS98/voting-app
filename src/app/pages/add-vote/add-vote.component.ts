import {Component, OnInit} from '@angular/core';
import {VotesService} from "../../core/services/votes.service";
import {CandidatesInterface} from "../../core/interfaces/candidates.interface";
import {VotersInterface} from "../../core/interfaces/voters.interface";
import {NotifierService} from "angular-notifier";

@Component({
  selector: 'app-add-vote',
  templateUrl: './add-vote.component.html',
  styleUrls: ['./add-vote.component.scss']
})
export class AddVoteComponent implements OnInit {

  public candidates: CandidatesInterface[];
  public voters: VotersInterface[];
  public selectedVoterId: number;
  public selectedCandidateId: number;

  constructor(private votesService: VotesService,
              private notifierService: NotifierService) {
  }

  ngOnInit(): void {
    this.prepareList();
  }

  private prepareList(): void {
    this.candidates = this.votesService.getCandidates();
    this.voters = this.votesService.getVoters().filter(voter => {
      return !voter.hasVoted;
    });
  }

  public saveVote(): void {
    if (!this.selectedVoterId || !this.selectedCandidateId) {
      this.notifierService.notify('error', 'Please select a voter and candidate.')
      return
    }
    this.selectedCandidateId = 0;
    this.selectedVoterId = 0;
    this.votesService.saveVote(this.selectedVoterId, this.selectedCandidateId);
    this.notifierService.notify('success', 'Added a voice.')
    this.prepareList();
  }
}
