export interface TableSettingsInterface {
  title: string;
  value: string;
  minWidth?: string;
}
