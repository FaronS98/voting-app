export interface CandidatesInterface {
  id?: number;
  name: string;
  surname: string;
  votes: number;
}
