export interface MenuInterface {
  name: string;
  routerLink: string;
}
