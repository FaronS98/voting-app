export interface VotersInterface {
  id?: number,
  name: string;
  hasVoted: boolean;
}
