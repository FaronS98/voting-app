import {FormGroup, FormControl, FormArray} from '@angular/forms';

export function getValidationMessage(validator: string, validatorValue?: any): string {
  const messages = {
    required: 'Wymagane',
  };
  // @ts-ignore
  return messages[validator];
}

export function validateAllFormFields(formGroup: FormGroup | FormArray) {
  Object.keys(formGroup.controls).forEach(field => {
    const control = formGroup.get(field);
    if (control instanceof FormControl || (control instanceof FormArray && !(control.controls.length > 0))) {
      control.markAsTouched({onlySelf: true});
      control.updateValueAndValidity({onlySelf: true});
    } else if ((control instanceof FormGroup) || (control instanceof FormArray)) {
      validateAllFormFields(control);
    }
  });
  formGroup.updateValueAndValidity();
}
