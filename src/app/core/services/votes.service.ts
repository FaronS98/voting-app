import {Injectable} from "@angular/core";
import voters from '../../../assets/data/voters.json'
import candidates from '../../../assets/data/candidates.json'
import {VotersInterface} from "../interfaces/voters.interface";
import {CandidatesInterface} from "../interfaces/candidates.interface";
import {AbstractControl, FormGroup} from "@angular/forms";

@Injectable({
  providedIn: "root"
})

export class VotesService {

  private voters: VotersInterface[] = voters;
  private candidates: CandidatesInterface[] = candidates;

  constructor() {
  }

  public getVoters(): VotersInterface[] {
    return this.voters;
  }

  public saveVoter(data: VotersInterface): void {
    this.voters.push(data);
  }

  public getCandidates(): CandidatesInterface[] {
    return this.candidates.sort((a: CandidatesInterface, b: CandidatesInterface) => b.votes - a.votes);
    ;
  }

  public saveCandidate(data: CandidatesInterface): void {
    this.candidates.push(data);
  }

  public saveVote(voterId: number, candidateId: number) {
    this.voters.forEach((voter, index) => {
      if (voter.id === voterId) {
        this.voters[index].hasVoted = true;
        return;
      }
    });
    this.candidates.forEach((candidate, index) => {
      if (candidate.id === candidateId) {
        this.candidates[index].votes++;
        return;
      }
    })
  }

  public resetForm(data: FormGroup): void {
    let control: AbstractControl | null = null;
    Object.keys(data.controls).forEach((name) => {
      control = data.controls[name];
      control.setValue(null);
      control.setErrors(null);
    });
    data.reset();
    data.markAsUntouched();
  }
}
