import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component, EventEmitter,
  Input,
  OnDestroy,
  OnInit, Output
} from '@angular/core';
import {FormControl} from "@angular/forms";
import {Subject, Subscription} from "rxjs";
import {getValidationMessage} from "../../core/validators/validators";

@Component({
  selector: 'app-input',
  templateUrl: './input.component.html',
  styleUrls: ['./input.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class InputComponent implements OnDestroy, OnInit {

  constructor(private ref: ChangeDetectorRef) {
  }

  ngOnInit() {
    this.handleChanges();
  }

  @Input() type: string = 'text';
  @Input() placeholder: string = '';
  @Input() span: string = '';
  @Input() formControlNameValue: FormControl;
  @Output() inputValue = new EventEmitter<string>();
  public error$: Subject<string> = new Subject();
  private subscription$: Subscription = new Subscription();

  private handleChanges(): void {
    if (this.formControlNameValue) {
      this.subscription$.add(this.formControlNameValue.statusChanges.subscribe(() => this.getError()));
    }
    this.subscription$.add(this.formControlNameValue.valueChanges.subscribe(() => {
      if (this.type === 'select') {
        this.sendInputValue(this.formControlNameValue.value);
      }
      this.ref.markForCheck();
    }));
  }

  public sendInputValue(value: string): void {
    this.inputValue.emit(value);
  }

  private getError(): void {
    if (this.formControlNameValue.errors && !this.formControlNameValue.errors.hasOwnProperty('required') && this.formControlNameValue.value === '') {
      this.error$.next(undefined);
      return;
    }
    if (this.formControlNameValue.touched) {
      if (this.formControlNameValue.errors) {
        const key = Object.keys(this.formControlNameValue.errors)[0];
        this.error$.next(getValidationMessage(key, this.formControlNameValue.errors[key]));
      } else {
        this.error$.next(undefined);
      }
    }
  }

  ngOnDestroy() {
    if (this.subscription$) {
      this.subscription$.unsubscribe();
    }
  }
}
