import {Component, Input} from '@angular/core';
import {MenuInterface} from "../../core/interfaces/menu.interface";

@Component({
  selector: 'app-header',
  templateUrl: './app-header.component.html',
  styleUrls: ['./app-header.component.scss']
})
export class AppHeaderComponent {

  @Input() topMenu: MenuInterface[];
}
