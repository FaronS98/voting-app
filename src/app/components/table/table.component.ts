import {Component, EventEmitter, Input, Output} from '@angular/core';
import {TableSettingsInterface} from "../../core/interfaces/table-settings.interface";

@Component({
  selector: 'app-table',
  templateUrl: './table.component.html',
  styleUrls: ['./table.component.scss']
})
export class TableComponent {

  @Input() header: string = '';
  @Input() tableSettings: TableSettingsInterface[];
  @Input() tableData = new Array();
  @Output() addElement = new EventEmitter<boolean>();

  public addNewElement(): void {
    this.addElement.emit(true);
  }
}
