import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';

@Component({
  selector: 'app-modal',
  templateUrl: './modal.component.html',
  styleUrls: ['./modal.component.scss']
})
export class ModalComponent implements OnInit {

  @Input() width: string = '';
  @Input() name: string = '';
  @Output() modalClosed = new EventEmitter();
  public visible: boolean = false;

  constructor() { }

  ngOnInit(): void {
  }

  public open(): void {
    this.visible = true;
  }

  public close(): void {
    if (this.visible) {
      this.modalClosed.emit();
    }
    this.visible = false;
  }
}
